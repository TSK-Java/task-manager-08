package ru.tsc.kirillov.tm.repository;

import ru.tsc.kirillov.tm.api.ICommandRepository;
import ru.tsc.kirillov.tm.constant.ArgumentConst;
import ru.tsc.kirillov.tm.constant.TerminalConst;
import ru.tsc.kirillov.tm.model.Command;

public class CommandRepository implements ICommandRepository {

    public static final Command ABOUT = new Command(
            TerminalConst.ABOUT,
            ArgumentConst.ABOUT,
            "Отображение информации о разработчике."
    );

    public static final Command VERSION = new Command(
            TerminalConst.VERSION,
            ArgumentConst.VERSION,
            "Отображение версии программы."
    );

    public static final Command HELP = new Command(
            TerminalConst.HELP,
            ArgumentConst.HELP,
            "Отображение доступных команд."
    );

    public static final Command EXIT = new Command(
            TerminalConst.EXIT,
            null,
            "Закрытие приложения."
    );

    public static final Command INFO = new Command(
            TerminalConst.INFO,
            ArgumentConst.INFO,
            "Отображение информации о системе."
    );

    public static final Command COMMANDS = new Command(
            TerminalConst.COMMANDS,
            ArgumentConst.COMMANDS,
            "Отображение списка команд."
    );

    public static final Command ARGUMENTS = new Command(
            TerminalConst.ARGUMENTS,
            ArgumentConst.ARGUMENTS,
            "Отображение списка аргументов."
    );

    public final static Command[] terminalCommands = new Command[] {
            ABOUT,
            VERSION,
            HELP,
            EXIT,
            INFO,
            COMMANDS,
            ARGUMENTS
    };

    @Override
    public Command[] getCommands() {
        return terminalCommands;
    }

}
