package ru.tsc.kirillov.tm;

import ru.tsc.kirillov.tm.api.ICommandRepository;
import ru.tsc.kirillov.tm.constant.ArgumentConst;
import ru.tsc.kirillov.tm.constant.TerminalConst;
import ru.tsc.kirillov.tm.model.Command;
import ru.tsc.kirillov.tm.repository.CommandRepository;
import ru.tsc.kirillov.tm.util.NumberUtil;

import java.util.Scanner;

public final class Application {

    private static final ICommandRepository COMMAND_REPOSITORY = new CommandRepository();

    public static void main(final String[] args) {
        if (processArgument(args))
            close();

        showWelcome();
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("\nВведите команду:");
            final String cmdText = scanner.nextLine();
            processCommand(cmdText);
        }
    }

    private static boolean processArgument(final String[] args) {
        if (args == null || args.length == 0)
            return false;

        final String firstArg = args[0];
        processArgument(firstArg);

        return true;
    }

    private static void processCommand(final String cmd) {
        if (cmd == null || cmd.isEmpty())
            return;
        switch (cmd) {
            case TerminalConst.ABOUT:
                showAbout();
                break;
            case TerminalConst.VERSION:
                showVersion();
                break;
            case TerminalConst.HELP:
                showHelp();
                break;
            case TerminalConst.EXIT:
                close();
                break;
            case TerminalConst.INFO:
                showSystemInfo();
                break;
            case TerminalConst.ARGUMENTS:
                showArguments();
                break;
            case TerminalConst.COMMANDS:
                showCommands();
                break;
            default:
                showUnexpectedCommand(cmd);
                break;
        }
    }

    private static void processArgument(final String arg) {
        if (arg == null || arg.isEmpty())
            return;
        switch (arg) {
            case ArgumentConst.ABOUT:
                showAbout();
                break;
            case ArgumentConst.VERSION:
                showVersion();
                break;
            case ArgumentConst.HELP:
                showHelp();
                break;
            case ArgumentConst.INFO:
                showSystemInfo();
                break;
            case ArgumentConst.ARGUMENTS:
                showArguments();
                break;
            case ArgumentConst.COMMANDS:
                showCommands();
                break;
            default:
                showUnexpectedArgument(arg);
                break;
        }
    }

    public static void close() {
        System.exit(0);
    }

    public static void showWelcome() {
        System.out.println("** Добро пожаловать в Task Manager **");
    }

    private static void showAbout() {
        showTerminalHeader(TerminalConst.ABOUT);
        System.out.println("Разработчик: Кириллов Максим");
        System.out.println("E-mail: mkirillov@tsconsulting.com");
    }

    private static void showHelp() {
        showTerminalHeader(TerminalConst.HELP);
        final Command[] commands = COMMAND_REPOSITORY.getCommands();
        for (final Command cmd: commands)
            System.out.println(cmd);
    }

    private static void showVersion() {
        showTerminalHeader(TerminalConst.VERSION);
        System.out.println("1.8.0");
    }

    private static void showUnexpectedCommand(final String cmd) {
        showTerminalHeader(TerminalConst.ERROR);
        System.err.printf("Команда `%s` не поддерживается.\n", cmd);
    }

    private static void showUnexpectedArgument(final String arg) {
        showTerminalHeader(TerminalConst.ERROR);
        System.err.printf("Аргумент `%s` не поддерживается.\n", arg);
    }

    private static void showTerminalHeader(final String name) {
        System.out.printf("[%s]\n", name != null ? name.toUpperCase() : "N/A");
    }

    private static void showSystemInfo() {
        final Runtime runtime = Runtime.getRuntime();
        final int availableProcessors = runtime.availableProcessors();
        final long freeMemory = runtime.freeMemory();
        final long maxMemory = runtime.maxMemory();
        final String maxMemoryFormat = maxMemory == Long.MAX_VALUE ? "безлимитно" : NumberUtil.formatBytes(maxMemory);
        final long totalMemory = runtime.totalMemory();
        final long usedMemory = totalMemory - freeMemory;

        System.out.println("Количество процесоров (ядер): " + availableProcessors);
        System.out.println("Свободно памяти: " + NumberUtil.formatBytes(freeMemory));
        System.out.println("Максимум памяти: " + maxMemoryFormat);
        System.out.println("Всего доступно памяти для JVM: " + NumberUtil.formatBytes(totalMemory));
        System.out.println("Используется памяти в JVM: " + NumberUtil.formatBytes(usedMemory));
    }

    private static void showCommands() {
        final Command[] commands = COMMAND_REPOSITORY.getCommands();
        for (final Command cmd: commands) {
            final String name = cmd.getName();
            if (name == null || name.isEmpty())
                continue;
            System.out.println(cmd.getName());
        }
    }

    private static void showArguments() {
        final Command[] commands = COMMAND_REPOSITORY.getCommands();
        for (final Command cmd: commands) {
            final String arg = cmd.getArgument();
            if (arg == null || arg.isEmpty())
                continue;
            System.out.println(arg);
        }
    }

}
