package ru.tsc.kirillov.tm.api;

import ru.tsc.kirillov.tm.model.Command;

public interface ICommandRepository {

    Command[] getCommands();

}
